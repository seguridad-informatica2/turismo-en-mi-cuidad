﻿using pryTurismoEnMiCiudad.Model.User;
using System.Collections.Generic;

namespace pryTurismoEnMiCiudad.BusinessRule.Security
{
    public interface itfSecurity {
        List<mdlUser> fncUserAuthentication(mdlUser objMdlUser);
        List<mdlUser> fncUserAuthenticationMorse(mdlUser objMdlUser);
    }
}
