﻿using pryTurismoEnMiCiudad.Model.User;
using pryTurismoEnMiCiudad.Security;
using System.Collections.Generic;

namespace pryTurismoEnMiCiudad.BusinessRule.Security
{
    public class clsSecurity : itfSecurity
    {
        private readonly Broker.security.itfSecurity objItfSecurity;
        private readonly itfSecurityEncryption objItfSecurityEncryption;

        public clsSecurity() {
            objItfSecurity = new Broker.security.optSecurity();
            objItfSecurityEncryption = new clsSecurityEncryption();
        }

        public List<mdlUser> fncUserAuthentication(mdlUser objMdlUser) {
            mdlUser objMdlUserEncryption = new mdlUser();

            string username =  objMdlUser.username;
            string password =  objMdlUser.password;

            username = objItfSecurityEncryption.fncCifradoAES(username, password);
            password = objItfSecurityEncryption.fncCifradoAES(password, password);

            objMdlUserEncryption.username = username;
            objMdlUserEncryption.password = password;

            return objItfSecurity.fncUserAuthentication(objMdlUserEncryption);
        }

        public List<mdlUser> fncUserAuthenticationMorse (mdlUser objMdlUser)
        {
            mdlUser objMdlUserEncryption = new mdlUser();

            string username = objMdlUser.username;
            string password = objMdlUser.password;

            username = objItfSecurityEncryption.encriptacionUnidimensional(username);
            password = objItfSecurityEncryption.encriptacionUnidimensional(password);

            objMdlUserEncryption.username = username;
            objMdlUserEncryption.password = password;

            return objItfSecurity.fncUserAuthentication(objMdlUserEncryption);

        }
    }
}
