﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pryTurismoEnMiCiudad.Model.User;

namespace pryTurismoEnMiCiudad.Broker.security
{
    public interface itfSecurity {
       List< mdlUser> fncUserAuthentication(mdlUser objMdlUser);
    }
}
