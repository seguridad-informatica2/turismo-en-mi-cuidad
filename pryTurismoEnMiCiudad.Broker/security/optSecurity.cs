﻿using pryTurismoEnMiCiudad.Model.User;

using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace pryTurismoEnMiCiudad.Broker.security
{
    public class optSecurity : itfSecurity {

       

        string strConnectionString = @"Data Source = (localdb)\MSSQLLocalDB;Initial Catalog = dbaTurismoEnMiCiudad; User ID = evelyn; Password=12345;Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public SqlConnection objConnect;
        public SqlCommand objCommand;
        public SqlDataReader objReader;
        public SqlDataAdapter objAdapter;
        public DataSet objDataSet;

        public spaSecurity objSecurity = new();

        public optSecurity() { }


        public List< mdlUser> fncUserAuthentication(mdlUser objMdlUser) {

            List<mdlUser> lstUser = new List<mdlUser>();

            try  {
                objConnect = new SqlConnection();
                objConnect.ConnectionString = strConnectionString;
                objConnect.Open();

                objCommand = new SqlCommand();
                objCommand.Connection = objConnect;
                objCommand.CommandText = objSecurity.spaUserAuthentication;
                objCommand.CommandType = CommandType.StoredProcedure;

                objCommand.Parameters.AddWithValue("@username", objMdlUser.username);
                objCommand.Parameters.AddWithValue("@password", objMdlUser.password);

                objReader = objCommand.ExecuteReader();

                if (objReader.HasRows) {
                    while (objReader.Read()) {
                        objMdlUser = new mdlUser();
                        objMdlUser.id = Guid.Parse(objReader["id"].ToString());
                        objMdlUser.name = objReader["name"].ToString();
                        objMdlUser.lastname = objReader["lastname"].ToString();
                        objMdlUser.address = objReader["address"].ToString();
                        objMdlUser.document = objReader["document"].ToString();
                        objMdlUser.cellphone = objReader["cellphone"].ToString();
                        objMdlUser.email = objReader["email"].ToString();
                        lstUser.Add(objMdlUser);
                    }

                } else {
                    objMdlUser = new mdlUser();
                    objMdlUser.result = "Aunticación No exitosa";
                    lstUser.Add(objMdlUser);
                }


            }catch(SqlException e)  {
                objMdlUser.result = e.Message;

            } finally {
                objReader.Close();
                objConnect.Close();

            }
            return lstUser;
        }
    }
}
