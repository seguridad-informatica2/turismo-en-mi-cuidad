﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pryTurismoEnMiCiudad.Model.User {

    public class mdlUser {
        public Guid id { get; set; }
        public string document { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string address { get; set; }
        public string cellphone { get; set; }
        public string email { get; set; }
        public bool status { get; set; }
        public DateTime date_updated { get; set; }
        public string operators { get; set; }
        public string result { get; set; }
    }
}
