﻿/*
Script de implementación para dbaTurismoEnMiCiudad

Una herramienta generó este código.
Los cambios realizados en este archivo podrían generar un comportamiento incorrecto y se perderán si
se vuelve a generar el código.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "dbaTurismoEnMiCiudad"
:setvar DefaultFilePrefix "dbaTurismoEnMiCiudad"
:setvar DefaultDataPath "C:\Users\pc6sb-3.USBMED\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"
:setvar DefaultLogPath "C:\Users\pc6sb-3.USBMED\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"

GO
:on error exit
GO
/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'Modificando Procedimiento [dbo].[user_autentication]...';


GO
ALTER PROCEDURE [dbo].[user_autentication]
	@username nvarchar(max) = null,
	@password nvarchar(max) = null
	
AS
	BEGIN 
		BEGIN TRY
			BEGIN TRANSACTION
			IF EXISTS(SELECT U.id 
						FROM users U
						WHERE U.username = @username COLLATE SQL_latin1_General_CP1_CI_AS)
			BEGIN 
				SELECT 
					U.id,
					U.username,
					U.password,
					U.name,
					U.lastname,
					U.document,
					U.address,
					U.cellphone,
					U.email
				FROM users U WITH(NOLOCK)
				WHERE U.username = @username COLLATE SQL_latin1_General_CP1_CI_AS
					AND U.password = @password COLLATE SQL_latin1_General_CP1_CI_AS
							
			END
			/*
			BEGIN 
				SELECT -1 AS RESULT
			END*/

			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION 
		END CATCH
	END
RETURN 0
GO
PRINT N'Actualización completada.';


GO
