﻿/*
Script de implementación para dbaTurismoEnMiCiudad

Una herramienta generó este código.
Los cambios realizados en este archivo podrían generar un comportamiento incorrecto y se perderán si
se vuelve a generar el código.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "dbaTurismoEnMiCiudad"
:setvar DefaultFilePrefix "dbaTurismoEnMiCiudad"
:setvar DefaultDataPath "C:\Users\pc6sb-3.USBMED\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"
:setvar DefaultLogPath "C:\Users\pc6sb-3.USBMED\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB\"

GO
:on error exit
GO
/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
PRINT N'La siguiente operación se generó a partir de un archivo de registro de refactorización b6d2c007-0c07-429f-beb2-66f06551afc2';

PRINT N'Cambiar el nombre de [dbo].[users].[operator] a operators';


GO
EXECUTE sp_rename @objname = N'[dbo].[users].[operator]', @newname = N'operators', @objtype = N'COLUMN';


GO
-- Paso de refactorización para actualizar el servidor de destino con los registros de transacciones implementadas
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = 'b6d2c007-0c07-429f-beb2-66f06551afc2')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('b6d2c007-0c07-429f-beb2-66f06551afc2')

GO

GO
PRINT N'Actualización completada.';


GO
