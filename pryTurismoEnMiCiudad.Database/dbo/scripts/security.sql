﻿USE [master]
GO
CREATE LOGIN [evelyn]
	WITH PASSWORD    = N'12345',
	DEFAULT_DATABASE = [master],
	CHECK_EXPIRATION = OFF,
	CHECK_POLICY     = OFF

GO

ALTER SERVER ROLE [sysadmin] ADD MEMBER [evelyn]

GO 

USE [dbaTurismoEnMiCiudad]

GO 

CREATE USER[evelyn] FOR LOGIN [evelyn]

GO

USE [dbaTurismoEnMiCiudad]
GO 
ALTER ROLE [db_owner] ADD MEMBER [evelyn]