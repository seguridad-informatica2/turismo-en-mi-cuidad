﻿CREATE PROCEDURE [dbo].[user_autentication]
	@username nvarchar(max) = null,
	@password nvarchar(max) = null
	
AS
	BEGIN 
		BEGIN TRY
			BEGIN TRANSACTION
			IF EXISTS(SELECT U.id 
						FROM users U
						WHERE U.username = @username COLLATE SQL_latin1_General_CP1_CI_AS)
			BEGIN 
				SELECT 
					U.id,
					U.username,
					U.password,
					U.name,
					U.lastname,
					U.document,
					U.address,
					U.cellphone,
					U.email
				FROM users U WITH(NOLOCK)
				WHERE U.username = @username COLLATE SQL_latin1_General_CP1_CI_AS
					AND U.password = @password COLLATE SQL_latin1_General_CP1_CI_AS
							
			END
			/*
			BEGIN 
				SELECT -1 AS RESULT
			END*/

			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION 
		END CATCH
	END
RETURN 0
