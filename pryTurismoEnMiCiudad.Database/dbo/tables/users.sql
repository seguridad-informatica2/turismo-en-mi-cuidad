﻿CREATE TABLE [dbo].[users]
(
	[id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY DEFAULT NEWID(), 
    [document] VARCHAR(MAX) NOT NULL, 
    [username] VARCHAR(MAX) NOT NULL, 
    [password] NVARCHAR(MAX) NULL, 
    [name] VARCHAR(MAX) NULL, 
    [lastname] NVARCHAR(MAX) NULL, 
    [address] NVARCHAR(MAX) NULL, 
    [cellphone] NVARCHAR(MAX) NULL, 
    [email] NVARCHAR(MAX) NULL, 
    [status] BIT NULL DEFAULT 1, 
    [date_updated] DATETIME NULL DEFAULT GetDate(), 
    [operators] NVARCHAR(MAX) NULL
)
