﻿using Microsoft.AspNetCore.Mvc;
using pryTurismoEnMiCiudad.BusinessRule.Security;
using pryTurismoEnMiCiudad.Model.User;
using System.Collections.Generic;

namespace pryTurismoEnMiCiudad.WA.Security.Controllers {

    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase {

        [HttpPost("fncUserAuthentication")]
        public List<mdlUser> fncUserAuthentication(mdlUser objMdlUser){

            itfSecurity objSecurity = new clsSecurity();
            return objSecurity.fncUserAuthentication(objMdlUser);
             
        }

        [HttpPost("fncUserAuthenticationMorse")]
        public List<mdlUser> fncUserAuthenticationMorse(mdlUser objMdlUser) {

            itfSecurity objSecurity = new clsSecurity();
            return objSecurity.fncUserAuthenticationMorse(objMdlUser);

        }

    }
}
