﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace pryTurismoEnMiCiudad.Security {

    public class clsSecurityEncryption : itfSecurityEncryption
    {

        public string encriptacionUnidimensional(string datoEntrada) {

        
            string mensajeCifradoTransposicion = "";
            string mensajeCrifradoSustitucion = "";

            int longitudDatoEntrada = datoEntrada.Length;
            char[] datos = datoEntrada.ToCharArray();
            char[] informacion = datos;
            char[] informacionCifradaTransposicion = new char[longitudDatoEntrada];
            char[] informacionCifradaSustitucion = new char[longitudDatoEntrada];

            int ancho = longitudDatoEntrada - 1;
            for (int posicion = 0; posicion < longitudDatoEntrada; posicion++ ) {
                informacionCifradaTransposicion[posicion] = datos[ancho];
                mensajeCifradoTransposicion += informacionCifradaTransposicion[posicion];
                Console.WriteLine(datos[posicion]);
                ancho--;
            }

            foreach(char letra in informacionCifradaTransposicion) {
                mensajeCrifradoSustitucion += buscarLetra(letra) + "  ";
                Console.WriteLine(mensajeCrifradoSustitucion);

            }

            return mensajeCrifradoSustitucion;
        }

        private string buscarLetra(char letra) {
            int tamanioAlfabeto = 49;
            string morse = "";
            string[] alfabetoMorse = new string[tamanioAlfabeto];
            alfabetoMorse[0] = "AÁaá*-";
            alfabetoMorse[1] = "Bb-***";
            alfabetoMorse[2] = "Cc-*-*";
            alfabetoMorse[3] = "Dd-**";
            alfabetoMorse[4] = "EÉeé*";
            alfabetoMorse[5] = "Ff**-*";
            alfabetoMorse[6] = "Gg--*";
            alfabetoMorse[7] = "Hh****";
            alfabetoMorse[8] = "IÍií**";
            alfabetoMorse[9] = "Jj*---";
            alfabetoMorse[10] = "Kk-*-";
            alfabetoMorse[11] = "Ll*-**";
            alfabetoMorse[12] = "Mm--";
            alfabetoMorse[13] = "NÑnñ-*";
            alfabetoMorse[14] = "OÓoó---";
            alfabetoMorse[15] = "Pp*--*";
            alfabetoMorse[16] = "Qq--*-";
            alfabetoMorse[17] = "Rr*-*";
            alfabetoMorse[18] = "Ss***";
            alfabetoMorse[19] = "Tt-";
            alfabetoMorse[20] = "UÚuú**-";
            alfabetoMorse[21] = "Vv***-";
            alfabetoMorse[22] = "Ww*--";
            alfabetoMorse[23] = "Xx-**--";
            alfabetoMorse[24] = "Yy-*--";
            alfabetoMorse[25] = "Zz--**";
            alfabetoMorse[26] = "1*----";
            alfabetoMorse[27] = "2**---";
            alfabetoMorse[28] = "3***--";
            alfabetoMorse[29] = "4****-";
            alfabetoMorse[30] = "5*****";
            alfabetoMorse[31] = "6-****";
            alfabetoMorse[32] = "7--****";
            alfabetoMorse[33] = "8---**";
            alfabetoMorse[34] = "9----*";
            alfabetoMorse[35] = "0-----";
            alfabetoMorse[36] = " ******";
            alfabetoMorse[37] = ".*-*-*-";
            alfabetoMorse[38] = ";-*-*-*";
            alfabetoMorse[39] = ":---***";
            alfabetoMorse[40] = ":---***";
            alfabetoMorse[41] = "?**--**";
            alfabetoMorse[42] = "!-*-*--";
            alfabetoMorse[43] = "(-*--*";
            alfabetoMorse[44] = ")-*--*-";
            alfabetoMorse[45] = "+*-*-*";
            alfabetoMorse[46] = "/-**-*";
            alfabetoMorse[47] = "@*--*-*";
            alfabetoMorse[48] = "=-***-";

            for (int i = 0; i < tamanioAlfabeto; i++) {
                string caracteres = alfabetoMorse[i];
                if (alfabetoMorse[i].Contains(letra)) {
                    if(i == 0 || i== 4 || i== 8 ||  i==13 || i == 15 || i == 21) {
                        morse = caracteres[4..];

                    } else {
                        if( i >= 26) {
                            morse = caracteres[1..];
                        } else {
                            morse = caracteres[2..];
                        }
                    }
                }
            }
           

            return morse;

        }

        public string fncCifradoBase64(string mensaje) {

            string mensajeCifrado = string.Empty;
            byte[] byteCifrado = Encoding.Unicode.GetBytes(mensaje);

            mensajeCifrado = Convert.ToBase64String(byteCifrado);

            return mensajeCifrado;
        }

        public string fncDecifradoBase64(string mensaje) {

            string mensajeDecifrado = string.Empty;
            byte[] byteDecifrado = Convert.FromBase64String(mensaje);

            mensajeDecifrado = Encoding.Unicode.GetString(byteDecifrado);

            return mensajeDecifrado;

        }

        public string fncCifradoBinario(string strMensaje)
        {
            StringBuilder strMensajeCifrado = new StringBuilder();
            var bytes = Encoding.Unicode.GetBytes(strMensaje);
            //var bytes = Encoding.UTF8.GetBytes(strMensaje);
            foreach (var t in bytes)
            {
                strMensajeCifrado.Append(Convert.ToString(t, 2).PadLeft(8, '0'));
            }
            return strMensajeCifrado.ToString();
        }

        public string fncDecifradoBinario(string strMensaje)
        {
            List<byte> strMensajeDecifrado = new List<byte>();
            for (int i = 0; i < strMensaje.Length; i += 8)
            {
                strMensajeDecifrado.Add(Convert.ToByte(strMensaje.Substring(i, 8), 2));
            }
            return Encoding.Unicode.GetString(strMensajeDecifrado.ToArray());
            //return Encoding.UTF8.GetString(strMensajeDecifrado.ToArray());
        }

        public string fncCifradoDecimal(string strMensaje)
        {
            StringBuilder strMensajeCifrado = new StringBuilder();
            var bytes = Encoding.Unicode.GetBytes(strMensaje);
            foreach (var t in bytes)
            {
                strMensajeCifrado.Append(Convert.ToString(t, 10).PadLeft(1, (char)32));
            }
            return strMensajeCifrado.ToString();
        }

        public string fncDecifradoDecimal(string strMensaje)
        {
            List<byte> strMensajeDecifrado = new List<byte>();
            for (int i = 0; i < strMensaje.Length; i++)
            {
                strMensajeDecifrado.Add(Convert.ToByte(strMensaje.Substring(i, 10), 2));
            }
            return Encoding.Unicode.GetString(strMensajeDecifrado.ToArray());
        }

        public string fncCifradoHexadecimal(string strMensaje)
        {
            StringBuilder strMensajeCifrado = new StringBuilder();
            var bytes = Encoding.Unicode.GetBytes(strMensaje);
            foreach (var t in bytes)
            {
                strMensajeCifrado.Append(t.ToString("X2"));
            }
            return strMensajeCifrado.ToString();
        }

        public string fncDecifradoHexadecimal(string strMensaje)
        {
            var bytes = new byte[strMensaje.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(strMensaje.Substring(i * 2, 2), 16);
            }
            return Encoding.Unicode.GetString(bytes);
        }

        public string fncCifradoOctal(string strMensaje)
        {
            StringBuilder strMensajeCifrado = new StringBuilder();
            var bytes = Encoding.Unicode.GetBytes(strMensaje);
            foreach (var t in bytes)
            {
                strMensajeCifrado.Append(Convert.ToString(t, 8));
            }
            return strMensajeCifrado.ToString();
        }

        public string fncDecifradoOctal(string strMensaje)
        {
            List<byte> strMensajeDecifrado = new List<byte>();
            for (int i = 0; i < strMensaje.Length; i += 8)
            {
                strMensajeDecifrado.Add(Convert.ToByte(strMensaje.Substring(i, 8), 2));
            }
            return Encoding.Unicode.GetString(strMensajeDecifrado.ToArray());
        }

        public string fncCifradoDES(string strMensaje, string strLlave)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            MD5 hashMD5 = MD5.Create();
            //MD5CryptoServicesProvider hashMD5 = new MD5CryptoServicesProvider();
            byte[] keyHash, buff;

            //keyHash = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strLlave));
            //keyHash = Encoding.ASCII.GetBytes(strLlave.Substring(0, 8));
            keyHash = ASCIIEncoding.ASCII.GetBytes(strLlave);
            des.Key = keyHash;
            des.Mode = CipherMode.ECB;
            des.Padding = PaddingMode.PKCS7;
            //des.IV = Encoding.ASCII.GetBytes("12345678");
            des.IV = Encoding.ASCII.GetBytes(strLlave.Substring(0, 8));

            buff = ASCIIEncoding.ASCII.GetBytes(strMensaje);
            return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buff, 0, buff.Length));
        }

        public string fncDecifradoDES(string strMensaje, string strLlave)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            MD5 hashMD5 = MD5.Create();
            //MD5CryptoServicesProvider hashMD5 = new MD5CryptoServicesProvider();
            byte[] keyHash, buff;

            //keyHash = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strLlave));
            //keyHash = Encoding.ASCII.GetBytes(strLlave.Substring(0, 8));
            keyHash = ASCIIEncoding.ASCII.GetBytes(strLlave);
            des.Key = keyHash;
            des.Mode = CipherMode.ECB;
            des.Padding = PaddingMode.PKCS7;
            //des.IV = Encoding.ASCII.GetBytes("12345678");
            des.IV = Encoding.ASCII.GetBytes(strLlave.Substring(0, 8));

            buff = Convert.FromBase64String(strMensaje);
            return ASCIIEncoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(buff, 0, buff.Length));
        }

        public string fncCifrado3DES(string strMensaje, string strLlave)
        {
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            MD5 hashMD5 = MD5.Create();
            //MD5CryptoServicesProvider hashMD5 = new MD5CryptoServicesProvider();
            byte[] keyHash, buff;

            keyHash = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strLlave));
            tdes.Key = keyHash;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            //des.IV = Encoding.ASCII.GetBytes("12345678");
            tdes.IV = Encoding.ASCII.GetBytes(strLlave.Substring(0, 8));

            buff = ASCIIEncoding.ASCII.GetBytes(strMensaje);
            return Convert.ToBase64String(tdes.CreateEncryptor().TransformFinalBlock(buff, 0, buff.Length));
        }

        public string fncDecifrado3DES(string strMensaje, string strLlave)
        {
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            MD5 hashMD5 = MD5.Create();
            //MD5CryptoServicesProvider hashMD5 = new MD5CryptoServicesProvider();
            byte[] keyHash, buff;

            keyHash = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strLlave));
            tdes.Key = keyHash;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            //des.IV = Encoding.ASCII.GetBytes("12345678");
            tdes.IV = Encoding.ASCII.GetBytes(strLlave.Substring(0, 8));

            buff = Convert.FromBase64String(strMensaje);
            return ASCIIEncoding.ASCII.GetString(tdes.CreateDecryptor().TransformFinalBlock(buff, 0, buff.Length));
        }

        public string fncCifradoAES(string strMensaje, string strLlave)
        {
            Aes aes = Aes.Create();
            MD5 hashMD5 = MD5.Create();
            byte[] keyHash, buff;

            keyHash = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strLlave));
            aes.Key = keyHash;
            aes.Mode = CipherMode.ECB;
            aes.Padding = PaddingMode.PKCS7;
            //aes.IV = Encoding.ASCII.GetBytes("1234567890123456");
            aes.IV = Encoding.ASCII.GetBytes(strLlave.Substring(0, 16));

            buff = ASCIIEncoding.ASCII.GetBytes(strMensaje);
            return Convert.ToBase64String(aes.CreateEncryptor().TransformFinalBlock(buff, 0, buff.Length));
        }

        public string fncDecifradoAES(string strMensaje, string strLlave)
        {
            Aes aes = Aes.Create();
            MD5 hashMD5 = MD5.Create();
            //MD5CryptoServicesProvider hashMD5 = new MD5CryptoServicesProvider();
            byte[] keyHash, buff;

            keyHash = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strLlave));
            aes.Key = keyHash;
            aes.Mode = CipherMode.ECB;
            aes.Padding = PaddingMode.PKCS7;
            //aes.IV = Encoding.ASCII.GetBytes("1234567890123456");
            aes.IV = Encoding.ASCII.GetBytes(strLlave.Substring(0, 16));

            buff = Convert.FromBase64String(strMensaje);
            return ASCIIEncoding.ASCII.GetString(aes.CreateDecryptor().TransformFinalBlock(buff, 0, buff.Length));
        }
    }
}
