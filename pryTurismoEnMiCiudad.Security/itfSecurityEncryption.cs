﻿

namespace pryTurismoEnMiCiudad.Security {

    public interface itfSecurityEncryption {

        string fncCifradoBase64(string mensaje);
        string fncDecifradoBase64(string mensaje);
        string fncCifradoAES(string strMensaje, string strLlave);
        string fncDecifradoAES(string strMensaje, string strLlave);
        public string encriptacionUnidimensional(string datoEntrada);

    }
}
